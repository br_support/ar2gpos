#!/usr/bin/env python
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import ssl
import datetime
import urllib.parse
import socketserver
import collections
import decimal
import re
# import threading
# import pyautogui
import os

def makeJsonResponse(status, message, response):
    data = {}
    data['status'] = status
    data['message'] = message
    data['response'] = response
    return json.dumps(data)

def debug_log(text):
    if len(args.l) > 0:
        try:
            with open(args.l, 'w+') as f:
                f = open(args.l, 'w+')
                f.write(makeJsonResponse(0, "",text))
        except:
            print('Can not log to file "{0}"'.format(args.l))

def debug_print(error, msg):
    print("Exception: code %s, message %s" % (str(error),msg))


class S(BaseHTTPRequestHandler):

#"""     __sqlDb = DB() """
    def _set_headers(self, contentLength):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.send_header("Content-Length", contentLength)
        self.send_header("Connection", "Keep-Alive")
        self.end_headers()

    def _respond(self, jsonResponse):
        self._set_headers(len(jsonResponse))
        self.wfile.write(bytes(jsonResponse, "utf-8"))

    def do_GET(self):
        self._set_headers(len("server up.."))
        self.wfile.write("server up..")

    def do_POST(self):
        # FIXME: handle invalid request
        length = int(self.headers.get('content-length'))
        r1 = self.rfile.read(length).decode('utf-8')
        data = urllib.parse.parse_qs(r1, keep_blank_values=1, encoding='utf-8')
        jsonRequest = list(data.items())[0][0]

        try:
            serialized = json.loads(jsonRequest)
        except Exception as ex:
            print('failed parsing {0}'.format(jsonRequest))
            self._respond(makeJsonResponse(1, ex.args[0], jsonRequest))
            return

        """
        if 'pyautogui.screenshot' in serialized:
            try:
                screenshotArguments = serialized['pyautogui.screenshot'][0]
                imageFilename = screenshotArguments['imageFilename']
                if 'region' in screenshotArguments:
                    region = tuple(screenshotArguments['region'])
                else:
                    region = None
                screenshot1 = pyautogui.screenshot(region)
                screenshot1.save(imageFilename)
                self._respond(makeJsonResponse(0, 'success', imageFilename))
            except Exception as ex:
                self._respond(makeJsonResponse(ex.args[0], ex.args[1], imageFilename))
        """
        
        if 'print' in serialized:
            try:
                printArguments = serialized['print'][0]
                filePath = printArguments['filePath']
                os.startfile(filePath,'print')
                print('File ' + filePath + ' printed with default settings')
                self._respond(makeJsonResponse(0, 'success', filePath))
            except Exception as ex:
                print('Print error: ' + ex.args[1])
                self._respond(makeJsonResponse(ex.args[0], ex.args[1], filePath))

def run(server_class=HTTPServer, handler_class=S, webServerPort=85):
    handler_class.protocol_version = 'HTTP/1.1'
    httpd = socketserver.ThreadingTCPServer(("",webServerPort),handler_class)
    print('Starting httpd at port ' + str(webServerPort))
    # FIXME: line below sets up HTTPS server, but it is args.sqlType yet supported from a client side
    # httpd.socket = ssl.wrap_socket (httpd.socket, certfile='./server.pem', server_side=True)
    httpd.serve_forever()

if __name__ == "__main__":
    import argparse
    __version__ = "V5.9.0"
    parser = argparse.ArgumentParser(
        description='This script works as a bridge between MpDatabase and defined SQL server',
        epilog='EXAMPLES:\n\n# start the script with default parameters (85, 127.0.0.1, 3306, mysql)\n$ python mappDatabaseConnector.py\n\n# start the script with defined parameters (e.g. 86, 192.168.1.15, 58964, mssql)\n$ python mappDatabaseConnector.py 86 \'192.168.1.15\' 58964 \'mssql\'\n\n# start the script with defined parameters (e.g. 86, 127.0.0.1, 5432, postgres)\n$ python mappDatabaseConnector.py 86 \'127.0.0.1\' 5432 \'postgres\' ',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('httpPort', type=str,
                    default='85', const=1, nargs='?',
                    help='http server port (default: 85)')
    parser.add_argument('sqlHost', type=str,
                    default='127.0.0.1', const=1, nargs='?',
                    help='sql server host (default: 127.0.0.1)')
    args = parser.parse_args()

    run(webServerPort=int(args.httpPort))