
TYPE
	Racer_typ : 	STRUCT 
		ID : UDINT;
		FinishTime : DATE_AND_TIME;
		Name : STRING[80];
		BestRound : REAL;
		FinalPosition : INT;
		Comment : WSTRING[80];
	END_STRUCT;
END_TYPE
