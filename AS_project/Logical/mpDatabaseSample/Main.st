PROGRAM _INIT
(* Insert code here *)
	MpDatabaseCore_0.MpLink := ADR(gDatabaseCore);
//	MpDatabaseCore_0.Enable := TRUE;
//	MpDatabaseCore_0.Connect := TRUE;
	MpDatabaseCore_0();
	MpDatabaseQuery_0.MpLink := ADR(gDatabaseCore);
	MpDatabaseQuery_0.Name := ADR(QueryName);
	MpDatabaseQuery_0.Enable := TRUE;
	MpDatabaseQuery_0();
	QueryName := 'select PV';
	
	tableName := 'table_1';
	
	RacerArray;
END_PROGRAM

PROGRAM _CYCLIC
	//get actual time
	DTGetTime_0(enable := TRUE);
	Racer.FinishTime := DTGetTime_0.DT1;

	MpDatabaseCore_0();
	MpDatabaseQuery_0();
END_PROGRAM

PROGRAM _EXIT
(* Insert code here *)
	MpDatabaseQuery_0.Enable := FALSE;
	MpDatabaseQuery_0();
	MpDatabaseCore_0.Enable := FALSE;
	MpDatabaseCore_0();
END_PROGRAM