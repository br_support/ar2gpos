#include <bur/plctypes.h>
#include <string.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{

}


void _CYCLIC ProgramCyclic(void)
{
	switch (step)
	{
		case 0:     
			step = 100;
			break;
		
		case 100:
			break;
		
		case 110:    
			// Screenshots (disabled)
			
			// strcpy(requestData, "{\"pyautogui.screenshot\":[{\"imageFilename\":\"C:/Temp/my_screenshot.png\",\"region\":[0,0, 300, 400]}]}");
			strcpy(requestData, "{\"pyautogui.screenshot\":[{\"imageFilename\":\"C:/Temp/my_screenshot.png\"}]}");

			httpClient_0.enable = 1;
			httpClient_0.send = 1;
			httpClient_0.method = httpMETHOD_POST;
			httpClient_0.option = httpOPTION_HTTP_11;
			httpClient_0.pHost = (UDINT)"127.0.0.1";
			httpClient_0.hostPort = 12000;
			httpClient_0.pUri = (UDINT)uri;
			httpClient_0.pRequestData = (UDINT)requestData;
			httpClient_0.requestDataLen = strlen(requestData);

			httpClient_0.pResponseData = (UDINT)responseData;
			httpClient_0.responseDataSize = sizeof(responseData);
			step = 200;
			break;
		
		
		case 120:
			// Printing
			strcpy(requestData, "{\"print\":[{\"filePath\":\"C:/Temp/Document.pdf\"}]}");
			
			httpClient_0.enable = 1;
			httpClient_0.send = 1;
			httpClient_0.method = httpMETHOD_POST;
			httpClient_0.option = httpOPTION_HTTP_11;
			httpClient_0.pHost = (UDINT)"127.0.0.1";
			httpClient_0.hostPort = 12000;
			httpClient_0.pUri = (UDINT)uri;
			httpClient_0.pRequestData = (UDINT)requestData;
			httpClient_0.requestDataLen = strlen(requestData);

			httpClient_0.pResponseData = (UDINT)responseData;
			httpClient_0.responseDataSize = sizeof(responseData);
			step = 200;
			break;
		
		case 200:
			httpClient_0.send = 0;
			step = 100;
			break;
	}
     
	httpClient(&httpClient_0);
}


void _EXIT ProgramExit(void)
{

}

